extends Node2D

var turn = true #True:Player False:machine
var cardsOnPlay = []
var activeCards = []
var frontRowCards = []
var backRowCards = []

var wave = 0
var activeEnemy
@export var enemies :Array[EnemyData]
var enemyScene = preload("res://Enemy/enemigo.tscn")

enum Fase{ENTER, EXIT, HIT, HITTED}
var fase
# Called when the node enters the scene tree for the first time.
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	$Player.setTurno(turn)
	$Player.startHand()
	spawnAnother()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func changeTurn():
	refillShield()
	if(turn):
		turn = false
		$Player.setTurno(turn)
		await activateCards()
		if activeEnemy.getHp() <= 0:
			activeEnemy.die()
			spawnAnother()
			await get_tree().create_timer(0.25).timeout
			playMachine()
			#$Player.victory()	
		else:	
			playMachine()
	else:
		turn = true	
		$Player.draw()
		$Player.setTurno(turn)
		print("PLAYER TURN")
	pass

func refillShield():
	activeEnemy.recharge()
	for card in activeCards:
		card.recharge()
	pass

func spawnAnother():

	var newEnemy = enemyScene.instantiate()
	newEnemy.mult = wave
	newEnemy.mData = enemies.pick_random()
	newEnemy.position = Vector2(0, -300)
	add_child(newEnemy)
	activeEnemy = newEnemy
	await activeEnemy.enter()
	wave += 1
	pass

func setDraw(CanDraw):
	$Player/Baraja.setCanDraw(CanDraw)
	pass


	
func playMachine():
	print("JUEGA LA MAQUINA")
	fase = Fase.HIT
	await enemyActivateEfect(activeEnemy)
	activeEnemy.attackAnim()
	await get_tree().create_timer(0.25).timeout
	activeEnemy.attackAnim()
	fase = Fase.HITTED
	if activeCards.size() > 0: 
		frontRowCards.clear()
		backRowCards.clear()
		for card in activeCards:
			if card.position.y < 300:
				frontRowCards.append(card)
			else:
				backRowCards.append(card)	
		if frontRowCards.size() > 0:
			await frontRowCards.pick_random().getDamaged(activeEnemy.getDamage())
		else:
			await backRowCards.pick_random().getDamaged(activeEnemy.getDamage())
	else:
		print("DAÑO A JUGADOR")
		$Player.getDamaged(activeEnemy.getDamage())
	if $Player.vida > 0:
			changeTurn()	
	pass

func activateCards():
	fase = Fase.HIT
	cardsOnPlay = get_tree().get_nodes_in_group("Card")
	for card in activeCards:
		print(card.getName())
		if(activeEnemy.getHp() > 0):
			card.position.y-=200
			card.setIsOnHand(true)
			activeEnemy.getDamaged(card.getDamage())	
			await get_tree().create_timer(0.25).timeout
			card.position.y+=200
			await activateEfect(card)
			card.setIsOnHand(false)
	pass

func activateEfect(card):
	await card.activate(fase)

func enemyActivateEfect(enemy):
	await enemy.activate(fase)

func addCard(card):
	activeCards.append(card)
	
func removeCard(card):
	activeCards.erase(card)	

func getActiveCards():
	return activeCards

func _on_skip_pressed():
	if(turn):
		changeTurn()
		$Player.resetPlay()	
	pass # Replace with function body.
