extends Node2D

var vida = 10
var turno = false
var maxTurnMovesBase = 2
var maxTurnMoves = 2
var turnMoves = 0
var maxHandSize = 10
var hand = 0
# Called when the node enters the scene tree for the first time.
func _ready():
	$HP.text = "HP: " + str(vida)
	$Plays.text = "ACTIONS: " + str(maxTurnMoves)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func setTurno(aTurno):
	turno = aTurno
	if(aTurno):
		$Plays.text = "ACTIONS: " + str(maxTurnMoves)
		turnMoves = 0	
	pass
	
func getTurno():
	return turno
	pass	

func startHand():
	$Baraja.initialHandDraw()	
	pass

func addCard():
	hand+=1


func cardPlayed():
	hand -=1
	turnMoves+=1
	
	$Plays.text = "ACTIONS: " + str(maxTurnMoves - turnMoves)
	
	if turnMoves >= maxTurnMoves:
		maxTurnMoves = maxTurnMovesBase
		get_parent().changeTurn()
	pass

func getDamaged(dmg):
	vida -= dmg 
	if vida <= 0:
		$HP.text = "HP: " + str("K.O.")
	else:	
		$HP.text = "HP: " + str(vida)
	pass

func victory():
	$HP.text = str("WIN")
	pass

func draw():
	if hand < maxHandSize:
		$Baraja.drawCard()
	pass
	
func heal(amount):
	vida += amount
	$HP.text = str(vida)
	
func addPlay(amount):
	maxTurnMoves += amount
	$Plays.text = "ACTIONS: " + str(maxTurnMoves)
		
func resetPlay():
	maxTurnMoves = maxTurnMovesBase
	$Plays.text = "ACTIONS: " + str(maxTurnMoves)
