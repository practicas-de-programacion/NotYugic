extends Resource
class_name EfectData

enum Type{ENTER, EXIT, HIT, HITTED}
enum Efect{DRAW, ATKUP, HEAL, PLAY, DAMAGE}
enum Target{PLAYER, CARD, ALL, ENEMY}

@export var Tipo: Type
@export var Efecto: Efect
@export var Objetivo: Target
@export var amount: int

