class_name Card
extends Node2D

#Data
@export var myData : CardData

#ATRIBUTOS
var Ataque
var Defensa
var Vida
var Efectos

#Texto
var Nombre
var Texto
var Imagen

#Logica
var selected = false
var zooming = false
var isOnHand = false
var isOnField = false
var rest_point
var myChild
var rest_nodes = []
var initialHand = false
enum Fase{ENTER, EXIT, HIT, HITTED}

var mColor

#Getters
func getNombre():
	return Nombre

func getTexto():
	return Texto
	
func getAtaque():
	return Ataque
	
func getDefensa():
	return Defensa
	
func getVida():
	return Vida	

func getEfectos():
	return Efectos

#Setters
func setTexto(newTexto):
	Texto = newTexto

func setAtaque(newAtaque):
	Ataque = newAtaque
	
func setDefensa(newDefensa):
	Defensa = newDefensa
	
func setVida(newVida):
	Vida = newVida
	
func SetEfectos(newEfectos):
	Efectos	= newEfectos
	
#Add and remove effects
func AddEfecto(newEfecto):
	if !Efectos.has(newEfecto):
		Efectos.append(newEfecto)

func RemoveEfecto(newEfeto):
	if Efectos.has(newEfeto):
		Efectos.erase(newEfeto)	
	
			

# Called when the node enters the scene tree for the first time.
func _ready():
	Nombre = myData.Nombre
	$Carta/Nombre.text = myData.Nombre
	Texto = myData.Texto
	$Carta/Texto.text = myData.Texto
	Ataque = myData.Ataque
	$Carta/Ataque.text = "ATK " + str(myData.Ataque)
	Defensa = myData.Defensa 
	$Carta/Defensa.text = "DEF " + str(myData.Defensa)
	Vida = myData.Vida
	$Carta/Vida.text = "HP " + str(myData.Vida)
	Efectos = myData.Efectos
	$Carta/FotoContainer/Foto.texture = myData.Imagen
	
	rest_nodes = get_tree().get_nodes_in_group("zone")
	rest_point = position
	mColor = $Carta/Fondo.color
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if selected && get_parent().getTurno():
		global_position = lerp(global_position, get_global_mouse_position(), 25 * delta)
	elif !zooming && !isOnHand:
		global_position = lerp(global_position, rest_point, 30 * delta)
	pass					


func _on_input_event(viewport, event, shape_idx):
	get_node("Carta").accept_event()
	initialHand = false
	if Input.is_action_just_pressed("click") && !zooming:
		selected = true
		z_index = 20
	if Input.is_action_just_pressed("zoom") && !selected: 	
		scale = Vector2(3,3)
		position = Vector2(300,-400)
		zooming = true
	pass # Replace with function body.
	
func _input(event):
	if Input.is_action_just_released("zoom"):
		scale = Vector2(1,1)
		zooming = false
	if Input.is_action_just_released("click"):	
		z_index = 10
		selected = false
		var shortest_dist = 100
		if !isOnField:
			for child in rest_nodes:
				var nChild = child as Zone
				var distance = global_position.distance_to(child.global_position + Vector2(100, 150))
				if distance < shortest_dist && !nChild.getOcupied():
					rest_point = child.global_position + Vector2(100, 150)
					isOnField = true
					if myChild != null:
						myChild.quitCard()
					shortest_dist = distance	
					myChild = nChild
					myChild.setCard()
					get_parent().get_parent().addCard(self)
					await activate(Fase.ENTER)
					get_parent().cardPlayed()
					
		
		

func setInitialHand(inhand):
	initialHand = inhand
	pass
	
func recharge():
	Defensa = myData.Defensa	
	$Carta/Defensa.text = "DEF " +  str(Defensa)
	
func _on_area_entered(area):
	if area.is_in_group("Hand") && !isOnField && !initialHand:
		isOnHand = true
	pass # Replace with function body.


func _on_area_exited(area):
	if area.is_in_group("Hand"):
		isOnHand = false
	pass # Replace with function body.

func getIsOnField():
	return isOnField
	pass
	
func getName():
	return Nombre
	pass	

func getDamaged(dmg):
	
	
	$Carta/Fondo.color = Color(1, 0, 0)
	await get_tree().create_timer(0.25).timeout
	$Carta/Fondo.color = mColor
	
	await activate(Fase.HITTED)	
	
	if(Defensa - dmg < 0):
		Vida -= (dmg -Defensa)
		Defensa = 0
		$Carta/Defensa.text = "DEF " +  str(0)
		$Carta/Vida.text = "HP " + str(Vida)
	else:	
		Defensa -= dmg
		$Carta/Defensa.text = "DEF " +  str(Defensa)
	if Vida <= 0:	
		isOnField = false
		get_parent().get_parent().removeCard(self)
		if myChild != null:
			myChild.quitCard()
			await activate(Fase.EXIT)
		queue_free()	
	pass

func setIsOnHand(ioh):
	isOnHand = ioh
	pass	
	
func getDamage():
	return Ataque
	pass	

func getEfects():
	return Efectos
	
func activate(fase):
	if Efectos.size() > 0:
		for efecto in Efectos:
			if efecto.Tipo == fase:
				await activateEfect(efecto)
	pass	


func activateEfect(efecto):
	match efecto.Efecto:
		efecto.Efect.DRAW:
			await draw(efecto)
			pass
		efecto.Efect.HEAL:	
			await heal(efecto)
			pass
		efecto.Efect.ATKUP:
			await BuffAtk(efecto)
			pass	
		efecto.Efect.PLAY:
			await AddPlay(efecto)	
			pass

func AddPlay(efecto):
	$Carta/Fondo.color = Color(1, 1, 1)
	await get_tree().create_timer(0.25).timeout
	$Carta/Fondo.color = mColor
	print("Play Extra:" + str(efecto.amount))
	await get_parent().addPlay(efecto.amount)
	pass

func draw(efecto):
	$Carta/Fondo.color = Color(1, 1, 0)
	await get_tree().create_timer(0.25).timeout
	$Carta/Fondo.color = mColor
	print("Draw:" + str(efecto.amount))
	for x in efecto.amount:
		await get_parent().draw()
	pass
	
	
func heal(efecto):
	print("Heal:" + str(efecto.amount))
	match efecto.Objetivo:
		efecto.Target.PLAYER:
			await get_parent().heal
		efecto.Target.CARD:
			await hpUP(efecto.amount)
		efecto.Target.ALL:
			for card in get_parent().get_parent().getActiveCards():
				await card.hpUP(efecto.amount)
	pass
	
	
func BuffAtk(efecto):
	print("ATKUP:" + str(efecto.amount))
	match efecto.Objetivo:
		efecto.Target.CARD:
			await atkUP(efecto.amount)
		efecto.Target.ALL:
			for card in get_parent().get_parent().getActiveCards():
				await card.atkUP(efecto.amount)
	pass		

func atkUP(amount):
	$Carta/Fondo.color = Color(0, 1, 1)
	await get_tree().create_timer(0.25).timeout
	$Carta/Fondo.color = mColor
	Ataque += amount
	$Carta/Ataque.text = "ATK " + str(Ataque)

func hpUP(amount):
	$Carta/Fondo.color = Color(0, 1, 0)
	await get_tree().create_timer(0.25).timeout
	$Carta/Fondo.color = mColor
	Vida += amount
	$Carta/Vida.text = "HP " + str(Vida)
