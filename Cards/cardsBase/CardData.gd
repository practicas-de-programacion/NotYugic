extends Resource
class_name CardData

@export var Vida: int = 0
@export var Ataque: int = 0
@export var Defensa: int = 0

@export var Nombre: String = "error"
@export var Texto: String = "not found"
@export var Imagen: Texture

@export var Efectos: Array[EfectData]
