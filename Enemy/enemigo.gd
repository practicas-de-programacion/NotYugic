extends Sprite2D

#Data
@export var mData : EnemyData

var hp
var atk
var def
var efects
var mult = 0

enum Fase{ENTER, EXIT, HIT, HITTED}

# Called when the node enters the scene tree for the first time.
func _ready():
	texture = mData.ImagenIdle	
	$Info/ColorRect/Nombre.text = mData.Nombre + " LVL: " + str(mult)
	$Info/ColorRect/Ataque.text = "ATK " + str(mData.Ataque+mult)
	atk = mData.Ataque+mult
	$Info/ColorRect/Defensa.text = "DEF " + str(mData.Defensa+(mult/2))
	def = mData.Defensa+(mult/2)
	$Info/ColorRect/Vida.text = "HP " + str(mData.Vida+(mult*2))
	hp = mData.Vida+(mult*2)
	efects = mData.Efectos

	#$Info.visible = false
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func getDamaged(dmg):
	
	modulate = Color(1, 0, 0)
	await get_tree().create_timer(0.5).timeout
	modulate = Color(1, 1, 1)
	
	await activate(Fase.HITTED)	
	
	if(def - dmg < 0):
		hp -= (dmg -def)
		def = 0
		$Info/ColorRect/Defensa.text = "DEF " + str(0)
		$Info/ColorRect/Vida.text = "HP " + str(hp)
	else:	
		def -= dmg
		$Info/ColorRect/Defensa.text = "DEF " + str(def)
	pass
	
func recharge():
	def = mData.Defensa+(mult/2)
	$Info/ColorRect/Defensa.text = "DEF " +  str(def)

func getDamage():
	return atk
	pass

func getHp():
	return hp

func die():
	await activate(Fase.EXIT)
	queue_free()
	pass

func attackAnim():
	if texture == mData.ImagenIdle:
		texture = mData.ImagenAtack
	else:
		texture = mData.ImagenIdle	
	pass

func enter():
	await activate(Fase.ENTER)
	pass

func activate(fase):
	if efects.size() > 0:
		for efecto in efects:
			if efecto.Tipo == fase:
				await activateEfect(efecto)
	pass	


func activateEfect(efecto):
	match efecto.Efecto:
		efecto.Efect.HEAL:	
			await heal(efecto)
			pass
		efecto.Efect.ATKUP:
			await BuffAtk(efecto)
			pass	
		efecto.Efect.PLAY:
			await AddPlay(efecto)	
			pass
		efecto.Efect.DAMAGE:
			await damage(efecto)

func damage(efecto):
	modulate = Color(0, 1, 0)
	await get_tree().create_timer(0.25).timeout
	modulate = Color(1, 1, 1)
	match efecto.Objetivo:
		efecto.Target.CARD:
			if get_parent().getActiveCards().size > 0:
				await get_parent().getActiveCards().pick_random().getDamaged(efecto.amount)
		efecto.Target.PLAYER:
			await get_parent().get_node("PLAYER").getDamaged(efecto.amount)
		efecto.Target.ALL:
			if get_parent().activeCards.size() > 0:
				for card in get_parent().getActiveCards():
					await card.getDamaged(efecto.amount)
	pass

func AddPlay(efecto):
	modulate = Color(0, 0, 0)
	await get_tree().create_timer(0.25).timeout
	modulate = Color(1, 1, 1)
	print("Play Less:" + str(efecto.amount))
	await get_parent().get_node("PLAYER").addPlay(efecto.amount * -1)
	pass
	
func heal(efecto):
	print("Enemy Heal:" + str(efecto.amount))
	hp += efecto.amount
	$Info/ColorRect/Vida.text = "HP " + str(hp)
	pass
	
	
func BuffAtk(efecto):
	print("Enemy ATKUP:" + str(efecto.amount))
	atk += efecto.amount
	$Info/ColorRect/Ataque.text = "ATK " + str(atk)
	pass	
