extends Resource
class_name EnemyData

@export var Vida: int = 0
@export var Ataque: int = 0
@export var Defensa: int = 0

@export var Nombre: String = "error"
@export var ImagenIdle: Texture
@export var ImagenAtack: Texture

@export var Efectos: Array[EfectData]
