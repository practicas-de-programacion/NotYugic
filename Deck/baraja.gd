extends Node2D

var cardScene = preload("res://Cards/cardsBase/card.tscn")
@export var Cartas: Array[CardData]
var cardIndex
var rng = RandomNumberGenerator.new()
var canDraw = true

# Called when the node enters the scene tree for the first time.
func _ready():
	cardIndex = 0
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

#func setCanDraw(canD):
#	canDraw = canD
#	pass
	
#func getCanDraw():
#	return canDraw
#	pass	

#func _on_input_event(viewport, event, shape_idx):
#	if Input.is_action_just_pressed("click") && get_parent().getTurno() && canDraw:
#		drawCard()
#		canDraw = false
#	pass # Replace with function body.

func drawCard():
	var newcard = cardScene.instantiate()
	newcard.myData = Cartas[cardIndex]
	newcard.position = Vector2(-750+rng.randf_range(-100,100),300+rng.randf_range(-100,100))
	newcard.rest_point = Vector2(-750,300)
	get_parent().add_child(newcard)
	get_parent().addCard()
	cardIndex+=1
	if(cardIndex > Cartas.size()-1):
		cardIndex = 0

func initialHandDraw():
	for c in 5:
		var newcard = cardScene.instantiate()
		newcard.myData = Cartas.pick_random()
		newcard.setInitialHand(true)
		newcard.position = Vector2(-1000+(c*120),300)
		newcard.rest_point = Vector2(-750,300)
		get_parent().add_child(newcard)
		get_parent().addCard()
		#cardIndex+=1
		#if(cardIndex > Cartas.size()-1):
		#	cardIndex = 0
		
		await get_tree().create_timer(0.25).timeout
		newcard.setIsOnHand(true)	
			
	pass
